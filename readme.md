## Details
Finds all files whose filenames are greater than specified length. The output is saved to a file named "outputfile.txt"

- While making backups of my hard drive, it used to fail in the middle as some of the filenames were extremely long and Windows didn't handle it properly. I didn't know which file it was. This is how this program was born.

## Usage
```
python files_with_longnames.py startpath filename_length

Eg:

Find all files whose filenames are greater than 30 characters.

python files_with_longnames.py /etc 30

Result: 
/etc/libreport/plugins/mantisbt_format_analyzer_libreport.conf
/etc/libreport/plugins/mantisbt_formatdup_analyzer_libreport.conf

```

## Author
Binaya Joshi
