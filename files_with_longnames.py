#!/usr/bin/env python
# Author: Binaya Joshi
# Finds files with long filenames and saves to a file
import os,sys

def main():
# make sure directory name is passed and also maximum filename is a number   
   if (len (sys.argv)==3 and sys.argv[2].isdigit() and os.path.isdir(sys.argv[1])):
   	fp1=open ("outputfile.txt","w")
  	max_len=int(sys.argv[2])
   	count=0
   	for dir, subdir,files in os.walk(sys.argv[1]):
           for j in files:
             if os.path.isfile(os.path.join(dir,j)) and (len(j.split(".")[0]) > max_len):
              count+=1
              longfiles= os.path.join(dir,j)
              print (longfiles)
              fp1.write (longfiles+"\n")
   	print "Total {} files found at {}".format (count, sys.argv[2])
   	fp1.close()
   else:
        print ("Usage: \n\t{} directory_name max_filelength".format (sys.argv[0]))
if __name__=="__main__":
      main()
    
